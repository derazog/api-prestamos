package com.bbva.hackaton.service.impl;

import com.bbva.hackaton.client.APIClientesRestClient;
import com.bbva.hackaton.client.APIGateway360RestClient;
import com.bbva.hackaton.constants.EstadoPrestamoEnum;
import com.bbva.hackaton.model.Cliente;
import com.bbva.hackaton.model.EstadoPrestamo;
import com.bbva.hackaton.model.Prestamo;
import com.bbva.hackaton.model.SmsResponse;
import com.bbva.hackaton.repository.PrestamoRepository;
import com.bbva.hackaton.service.PrestamoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PrestamoServiceImpl implements PrestamoService {

    @Autowired
    PrestamoRepository prestamoRepository;

    @Autowired
    APIClientesRestClient apiClientesRestClient;

    @Autowired
    APIGateway360RestClient apiGateway360RestClient;

    @Override
    public Prestamo registrarPrestamo(Prestamo prestamo) {
        Prestamo prestamoInsertado = null;

        //Solo recibimos el codCentral del cliente
        if (prestamo.getCliente() != null && !prestamo.getCliente().getCodCentral().isEmpty()) {
            //Completamos los demas campos con el API Clientes
            String codCentral = prestamo.getCliente().getCodCentral();
            System.out.println("[registrarPrestamo] cliente.codCentral = " + codCentral);
            Cliente cliente = apiClientesRestClient.obtenerClientePorCodCentral(codCentral);
            if (cliente != null) {
                System.out.println("[registrarPrestamo] cliente recuperado " + cliente);
                //Seteamos el cliente en el objeto Prestamo
                prestamo.setCliente(cliente);
                prestamoInsertado = prestamoRepository.insert(prestamo);
            } else {
                System.out.println("[registrarPrestamo] cliente no encontrado");
            }
        } else {
            System.out.println("[registrarPrestamo] cliente nulo");
        }

        return prestamoInsertado;
    }

    @Override
    public Prestamo actualizarEstadoPrestamo(String idPrestamo, EstadoPrestamo estadoPrestamo) {
        Prestamo prestamoActualizado = null;
        Optional<Prestamo> optional = this.prestamoRepository.findById(idPrestamo);
        if (optional.isPresent()) {
            Prestamo prestamo = optional.get();
            prestamo.setEstado(estadoPrestamo.getCodEstadoNuevo());
            prestamoActualizado = this.prestamoRepository.save(prestamo);

            //enviamos el SMS al Cliente
            if (prestamo.getCliente() != null) {
                String celular = prestamo.getCliente().getCelular();
                String nomCliente = prestamo.getCliente().getNombre();
                String descEstadoPrestamo = EstadoPrestamoEnum.valueOf(estadoPrestamo.getCodEstadoNuevo()).codEstado;
                System.out.println("[actualizarEstadoPrestamo] celular = " + celular);
                System.out.println("[actualizarEstadoPrestamo] nomCliente = " + nomCliente);
                System.out.println("[actualizarEstadoPrestamo] descEstadoPrestamo = " + descEstadoPrestamo);
                SmsResponse smsResponse = apiGateway360RestClient.enviarSms(celular, nomCliente, descEstadoPrestamo);

                if (smsResponse == null || !"ok".equals(smsResponse.getStatus())) {
                    prestamoActualizado = null;
                    System.out.println("[actualizarEstadoPrestamo] error al enviar SMS");
                } else {
                    System.out.println("[actualizarEstadoPrestamo] smsResponse = " + smsResponse);
                }
            } else {
                prestamoActualizado = null;
                System.out.println("[actualizarEstadoPrestamo] error al enviar SMS");
            }
        }

        return prestamoActualizado;
    }

    @Override
    public Prestamo obtenerPrestamoPorId(String idPrestamo) {
        final Optional<Prestamo> p = this.prestamoRepository.findById(idPrestamo);
        return p.isPresent() ? p.get() : null;
    }

    @Override
    public List<Prestamo> obtenerPrestamoPorNumDoc(String tipoDoc, String numDoc) {
        List<Prestamo> prestamos = prestamoRepository.findAll();
        return prestamos.stream()
                .filter(p -> p.getCliente() != null && numDoc.equals(p.getCliente().getNumDoc())
                        && tipoDoc.equals(p.getCliente().getTipoDoc()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Prestamo> obtenerPrestamoPorContrato(String codContrato) {
        List<Prestamo> prestamos = prestamoRepository.findAll();
        return prestamos.stream()
                .filter(p -> p.getContrato() != null && codContrato.equals(p.getContrato().getCodContrato()))
                .collect(Collectors.toList());
    }
}
