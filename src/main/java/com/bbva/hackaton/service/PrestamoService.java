package com.bbva.hackaton.service;

import com.bbva.hackaton.model.EstadoPrestamo;
import com.bbva.hackaton.model.Prestamo;
import java.util.List;

public interface PrestamoService {

    //POST /prestamos
    Prestamo registrarPrestamo(Prestamo prestamo);

    //GET /prestamos/{idPrestamo}
    public Prestamo obtenerPrestamoPorId(String idPrestamo);

    //GET /prestamos?tipoDoc=XXX&&numDoc=XXX
    List<Prestamo> obtenerPrestamoPorNumDoc(String tipoDoc, String numDoc);

    //GET /prestamos?codContrato=XXX
    List<Prestamo> obtenerPrestamoPorContrato(String codContrato);

    //PATCH /prestamos/{idPrestamo} @RequestBody
    Prestamo actualizarEstadoPrestamo(String idPrestamo, EstadoPrestamo estadoPrestamo);
}
