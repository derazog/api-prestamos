package com.bbva.hackaton.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesUtil {

    @Value("${api.clientes.url}")
    public String API_CLIENTES_URL;

    @Value("${api.gateway360.url}")
    public String API_GATEWAY360_URL;

    @Value("${api.gateway360.api.key}")
    public String API_KEY;

    @Value("${api.gateway360.report.url}")
    public String REPORT_URL;

    @Value("${api.gateway360.concat}")
    public Integer CONCAT;

    @Value("${api.gateway360.from}")
    public String FROM_SMS;

    @Value("${api.gateway360.template.sms}")
    public String TEMPLATE_SMS;
}
