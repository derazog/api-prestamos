package com.bbva.hackaton.client;

import com.bbva.hackaton.model.Message;
import com.bbva.hackaton.model.SmsRequest;
import com.bbva.hackaton.model.SmsResponse;
import com.bbva.hackaton.util.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class APIGateway360RestClient {

    @Autowired
    PropertiesUtil propertiesUtil;

    public SmsResponse enviarSms(String celular, String nomCliente, String estadoPrestamo) {
        SmsResponse smsResponse = null;

        try {
            RestTemplate restTemplate = new RestTemplate();
            SmsRequest smsRequest = new SmsRequest();
            smsRequest.setApi_key(propertiesUtil.API_KEY);
            smsRequest.setReport_url(propertiesUtil.REPORT_URL);
            smsRequest.setConcat(propertiesUtil.CONCAT);
            Message message = new Message();
            message.setFrom(propertiesUtil.FROM_SMS);
            String PREFIX_PERU = "51";
            message.setTo(PREFIX_PERU + celular);
            message.setText(MessageFormat.format(propertiesUtil.TEMPLATE_SMS, nomCliente, estadoPrestamo));
            Date date = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            String strDate = dateFormat.format(date);
            message.setSent_at(strDate);
            List<Message> messages = new ArrayList<>();
            messages.add(message);
            smsRequest.setMessages(messages);
            System.out.println("[enviarSms] smsRequest = " + smsRequest);
            smsResponse = restTemplate.postForObject(propertiesUtil.API_GATEWAY360_URL, smsRequest, SmsResponse.class);

        } catch (Exception e) {
            System.out.println("[enviarSms] Ocurrio un error al consumir el API Gateway360");
            e.printStackTrace();
        }

        return smsResponse;
    }
}
