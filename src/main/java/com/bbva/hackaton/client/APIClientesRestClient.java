package com.bbva.hackaton.client;

import com.bbva.hackaton.model.Cliente;
import com.bbva.hackaton.util.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class APIClientesRestClient {

    @Autowired
    PropertiesUtil propertiesUtil;

    public Cliente obtenerClientePorCodCentral(String codCentral) {
        Cliente cliente = null;

        try {
            RestTemplate restTemplate = new RestTemplate();
            cliente = restTemplate.getForObject(propertiesUtil.API_CLIENTES_URL + "/" + codCentral, Cliente.class);
        } catch (Exception e) {
            System.out.println("[obtenerClientePorCodCentral] Ocurrio un error al consumir el API Clientes");
            e.printStackTrace();
        }

        return cliente;
    }
}
