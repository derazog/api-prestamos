package com.bbva.hackaton.model;

import java.util.List;

public class SmsResponse {

    private String status;
    private List<Result> result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "SmsResponse {" +
                "status='" + status + '\'' +
                ", result=" + result + '}';
    }
}
