package com.bbva.hackaton.model;

public class Cliente {

    private String codCentral;
    private String nombre;
    private String apellidoMat;
    private String apellidoPat;
    private String tipoDoc;
    private String numDoc;
    private String celular;

    public Cliente(){

    }

    public String getCodCentral() {
        return codCentral;
    }

    public void setCodCentral(String codCentral) {
        this.codCentral = codCentral;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoMat() {
        return apellidoMat;
    }

    public void setApellidoMat(String apellidoMat) {
        this.apellidoMat = apellidoMat;
    }

    public String getApellidoPat() {
        return apellidoPat;
    }

    public void setApellidoPat(String apellidoPat) {
        this.apellidoPat = apellidoPat;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Override
    public String toString() {
        return "Cliente {" + "codCentral='" + codCentral + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellidoMat='" + apellidoMat + '\'' +
                ", apellidoPat='" + apellidoPat + '\'' +
                ", tipoDoc='" + tipoDoc + '\'' +
                ", numDoc='" + numDoc + '\'' +
                ", celular='" + celular + '\'' + '}';
    }
}
