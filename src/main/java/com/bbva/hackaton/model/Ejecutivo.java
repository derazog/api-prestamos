package com.bbva.hackaton.model;

public class Ejecutivo {

    private String codEjecutivo;
    private String codOficina;
    private String nombres;

    public String getCodEjecutivo() {
        return codEjecutivo;
    }

    public void setCodEjecutivo(String codEjecutivo) {
        this.codEjecutivo = codEjecutivo;
    }

    public String getCodOficina() {
        return codOficina;
    }

    public void setCodOficina(String codOficina) {
        this.codOficina = codOficina;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
}
