package com.bbva.hackaton.model;

public class EstadoPrestamo {

    private String codEstadoNuevo;

    public String getCodEstadoNuevo() {
        return codEstadoNuevo;
    }

    public void setCodEstadoNuevo(String codEstadoNuevo) {
        this.codEstadoNuevo = codEstadoNuevo;
    }
}
