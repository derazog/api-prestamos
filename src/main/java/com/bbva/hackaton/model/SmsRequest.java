package com.bbva.hackaton.model;

import java.util.List;

public class SmsRequest {

    private String api_key;
    private String report_url;
    private Integer concat;
    private List<Message> messages;

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getReport_url() {
        return report_url;
    }

    public void setReport_url(String report_url) {
        this.report_url = report_url;
    }

    public Integer getConcat() {
        return concat;
    }

    public void setConcat(Integer concat) {
        this.concat = concat;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    @Override
    public String toString() {
        return "SmsRequest {" +
                "api_key='" + api_key + '\'' +
                ", report_url='" + report_url + '\'' +
                ", concat=" + concat +
                ", messages=" + messages + '}';
    }
}
