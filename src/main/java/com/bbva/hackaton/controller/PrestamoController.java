package com.bbva.hackaton.controller;

import com.bbva.hackaton.model.EstadoPrestamo;
import com.bbva.hackaton.model.Prestamo;
import com.bbva.hackaton.service.PrestamoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/prestamos")
public class PrestamoController {

    @Autowired
    PrestamoService prestamoService;

    @PostMapping
    public ResponseEntity<?> postProductos(@RequestBody Prestamo prestamo) {
        Prestamo p = prestamoService.registrarPrestamo(prestamo);
        if (p != null){
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{idPrestamo}")
    public ResponseEntity<Prestamo> obtenerPrestamo(@PathVariable String idPrestamo){
        final Prestamo p = this.prestamoService.obtenerPrestamoPorId(idPrestamo);
        if (p != null){
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PatchMapping("/{idPrestamo}")
    public ResponseEntity<Prestamo> obtenerPrestamo(@PathVariable String idPrestamo,
                                                    @RequestBody EstadoPrestamo estadoPrestamo) {
        final Prestamo p = this.prestamoService.actualizarEstadoPrestamo(idPrestamo, estadoPrestamo);
        if (p != null){
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping
    public ResponseEntity<List<Prestamo>> obtenerPrestamoPorParametrosCliente(
            @RequestParam(value = "tipoDoc", required = false) String tipoDoc,
            @RequestParam(value = "numDoc", required = false) String numDoc,
            @RequestParam(value = "codContrato", required = false) String codContrato) {
        List<Prestamo> prestamos;
        if (codContrato != null) {
            prestamos = this.prestamoService.obtenerPrestamoPorContrato(codContrato);
        } else {
            prestamos = this.prestamoService.obtenerPrestamoPorNumDoc(tipoDoc, numDoc);
        }
        if (prestamos != null && prestamos.size() > 0) {
            return ResponseEntity.ok(prestamos);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
