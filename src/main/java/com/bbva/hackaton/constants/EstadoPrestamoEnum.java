package com.bbva.hackaton.constants;

public enum EstadoPrestamoEnum {

    EVAL("EVALUACION"),
    APR("APROBACION"),
    DOC("ENVIO DE DOCUMENTOS"),
    TAS("TASACION"),
    SUB("SUBSANACION"),
    DES("DESEMBOLSO"),
    NOT("NOTARIA");

    public final String codEstado;

    private EstadoPrestamoEnum(String codEstado) {
        this.codEstado = codEstado;
    }
}
